import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Buckets from './components/Buckets'
import Files from './components/Files'
import Stream from './components/Stream'


ReactDOM.render(

    <BrowserRouter>
        <Switch>
	        <Route path="/buckets" component={Buckets}/>
            <Route path="/files/:bucketId" component={Files}/>
            <Route path="/stream/:bucketId/:fileId" component={Stream}/>
        </Switch>
    </BrowserRouter>

    , document.getElementById('root'));

registerServiceWorker();


