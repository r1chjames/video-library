import axios from 'axios';
import {AUTH, API} from './ApiConfig.js'


 export function getBuckets() {
        return axios.get(API + 'buckets' + AUTH)
            .then(response => response.data)
    };

  export function getFiles(bucketId) {
        return axios.get(API + 'files/' + bucketId + AUTH)
            .then(response => response.data)
    };
