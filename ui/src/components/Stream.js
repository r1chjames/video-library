import React, {Component} from 'react';
import Header from './Header'
import { AUTH, API } from '../ApiConfig.js'
import { DefaultPlayer as Video } from 'react-html5video';
import 'react-html5video/dist/styles.css';

class Stream extends Component {

    constructor(props) {
        super(props);

        this.state = {
          isLoading: false,
          error: null
        };
    }

	render() {

      const { match: { params } } = this.props;
	  const { isLoading, error } = this.state;

	  if (error) {
		return <p>{error.message}</p>;
	  }

	  if (isLoading) {
		return (
            <div className = "App">
	            <Header />
                <p>Loading ...</p>
	        </div>
	)}

	return (
	 <Video autoPlay loop muted
                controls={['PlayPause', 'Seek', 'Time', 'Volume', 'Fullscreen']}
                poster="http://sourceposter.jpg"
                onCanPlayThrough={() => {
                    // Do stuff
                }}>
                <source src={API + "files/" + params.bucketId + "/stream/" + params.fileId + AUTH} type="video/mp4" />
                <track label="English" kind="subtitles" srcLang="en" src="http://source.vtt" default />
            </Video>
//	    <div className = "App">
//	    <Header />
//            <video id="videoPlayer" controls>
//                <source src={API + "files/" + params.bucketId + "/stream/" + params.fileId + AUTH} type="video/mp4" />
//            </video>
//	    </div>
	);
  }
}

export default Stream