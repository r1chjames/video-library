import React from 'react';
import { Link, Route, Switch } from 'react-router-dom';
import Buckets from './Buckets'
import Files from './Files'

const Main = () => (
//    <main>
//        <Switch>
	        <Route path="/" component={Buckets}/>
            <Route path="/files" component={Files}/>
//        </Switch>
//    </main>
)

export default Main