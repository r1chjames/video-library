import React, {Component} from 'react';
import Header from './Header'
import {ListGroup, ListGroupItem} from 'react-bootstrap';
import { WEB } from '../ApiConfig.js'
import { getFiles } from '../Api.js'

class Files extends Component {

    constructor(props) {
        super(props);

        this.state = {
          content: [],
          isLoading: false,
          error: null
        };
      }

  async componentDidMount() {
    const { match: { params } } = this.props;

	 this.setState({ isLoading: true });

     const files = await getFiles(params.bucketId);
     this.setState({content: files, isLoading: false});

  }

	render() {
	  const { match: { params } } = this.props;
	  const { content, isLoading, error } = this.state;

	  if (error) {
		return <p>{error.message}</p>;
	  }

	  if (isLoading) {
		return (
            <div className = "App">
	            <Header />
                <p>Loading ...</p>
	        </div>
	)}

	return (
	    <div className = "App">
	    <Header />
            <ListGroup> {
                content.map(file =>
                  <ListGroupItem
                    header={file.filename}
                    href={ WEB + 'stream/' + params.bucketId + '/' + file.fileId }>
                    {file.fileId}
                  </ListGroupItem>
                )}
            </ListGroup>
	    </div>
	);
  }
}

export default Files