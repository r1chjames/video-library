import React, {Component} from 'react';
import Header from './Header'
import {ListGroup, ListGroupItem} from 'react-bootstrap';
import { WEB } from '../ApiConfig.js'
import { getBuckets } from '../Api.js'

class Buckets extends Component {

constructor(props) {
	super(props);

	this.state = {
	  content: [],
	  isLoading: false,
	  error: null
	};
  }

  async componentDidMount() {
	 this.setState({ isLoading: true });

     const buckets = await getBuckets();
     this.setState({content: buckets, isLoading: false});

  }

	render() {
	  const { content, isLoading, error } = this.state;

	  if (error) {
		return <p>{error.message}</p>;
	  }

	  if (isLoading) {
		return (
            <div className = "App">
	            <Header />
                <p>Loading ...</p>
	        </div>
	)}

	return (
	    <div className = "App">
	    <Header />
            <ListGroup> {
                content.map(bucket =>
                  <ListGroupItem
                    header={bucket.bucketName}
                    href={ "" + WEB + 'files/' + bucket.bucketId }>
                    {bucket.bucketId}
                  </ListGroupItem>
                )}
            </ListGroup>
	    </div>
	);
  }
}

export default Buckets