package com.r1chjames.controllers

import com.r1chjames.config.ApplicationConfig
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.test.FakeRequest
import play.api.test.Helpers._

/**
  * Integration test to run through event source.
  */
class FileControllerTest extends PlaySpec
  with GuiceOneAppPerSuite
  with ScalaFutures {

  val config = ApplicationConfig()
  val accountId = config.accountId
  val applicationKey = config.applicationKey

  "file controller" should {
    "return index page" in {
      val request = FakeRequest(method = GET, path = "/")
      val home = route(app, request).get

      contentAsString(home) must include("This is a placeholder page to show you the REST API.")
    }

    "return all buckets" in {
      val request = FakeRequest(method = GET, path = s"/api/v1/buckets?accountId=$accountId&applicationKey=$applicationKey")
      route(app, request) match {
        case Some(future) =>
          whenReady(future) { result =>
            result.header.status mustEqual OK
          }
        case None =>
          fail
      }
    }

    "return all files in a bucket" in {
      val request = FakeRequest(method = GET, path = s"/api/v1/files/b6702043f80ce8b063060217?accountId=$accountId&applicationKey=$applicationKey")
      route(app, request) match {
        case Some(future) =>
          whenReady(future) { result =>
            result.header.status mustEqual OK
          }
        case None =>
          fail
      }
    }

    "return all files for a date" in {
      val request = FakeRequest(method = GET, path = s"/api/v1/files/b6702043f80ce8b063060217/2018-07-22_08?accountId=$accountId&applicationKey=$applicationKey")
      route(app, request) match {
        case Some(future) =>
          whenReady(future) { result =>
            result.header.status mustEqual OK
          }
        case None =>
          fail
      }
    }

    "stream a file" in {
      val request = FakeRequest(method = GET, path = s"/api/v1/files/b6702043f80ce8b063060217/stream/4_zb6702043f80ce8b063060217_f106d06d1a46bd645_d20180722_m090009_c001_v0001030_t0008?accountId=$accountId&applicationKey=$applicationKey")
      route(app, request) match {
        case Some(future) =>
          whenReady(future) { result =>
            result.header.status mustEqual PARTIAL_CONTENT
          }
        case None =>
          fail
      }
    }

    "download a file" in {
      val request = FakeRequest(method = POST, path = s"/api/v1/files/b6702043f80ce8b063060217/download/4_zb6702043f80ce8b063060217_f106d06d1a46bd645_d20180722_m090009_c001_v0001030_t0008?accountId=$accountId&applicationKey=$applicationKey")
      route(app, request) match {
        case Some(future) =>
          whenReady(future) { result =>
            result.header.status mustEqual ACCEPTED
          }
        case None =>
          fail
      }
    }
  }
}