package com.r1chjames

import com.r1chjames.components.MasterComponent
import com.r1chjames.repos.StorageRepo

trait MockComponentImpl extends MasterComponent {
  val storageRepo: StorageRepo = new MockStorageRepoImpl()
}