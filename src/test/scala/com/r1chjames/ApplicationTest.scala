package com.r1chjames

import java.time.LocalDateTime

import com.r1chjames.models.{Bucket, File, FileTypeEnumeration}
import org.mockito.Mockito._
import org.scalatest.FunSuite
import org.scalatest.mockito.MockitoSugar

class ApplicationTest extends FunSuite with MockitoSugar {

  val bucketId = "test-bucket"
  val date = LocalDateTime.now
  val storageRepo = mock[MockStorageRepoImpl]

  test("testListFilesForDate") {
    when(storageRepo.listFilesForDate(bucketId, date)).thenReturn(List(singleMockFile))
    val filesReturned = storageRepo.listFilesForDate(bucketId, date)
    assert(List(singleMockFile) == filesReturned)
    verify(storageRepo, times(1)).listFilesForDate(bucketId, date)
  }

  test("testDownloadFile") {
    val fileId = "test-file-1"
    storageRepo.downloadFile(fileId)
    verify(storageRepo, times(1)).downloadFile(fileId)

  }

  test("testListFilesInBucket") {
    when(storageRepo.listFilesInBucket(bucketId)).thenReturn(mockFiles())
    val filesReturned = storageRepo.listFilesInBucket(bucketId)
    assert(mockFiles() == filesReturned)
    verify(storageRepo, times(1)).listFilesInBucket(bucketId)

  }

  test("testListBuckets") {
    when(storageRepo.listBuckets).thenReturn(listBuckets)
    val bucketsReturned = storageRepo.listBuckets
    assert(listBuckets == bucketsReturned)
    verify(storageRepo, times(1)).listBuckets

  }

  def listBuckets(): List[Bucket] = List(Bucket("zzzz-99999", "test-bucket-1", "B2", Map.empty[String, String]), Bucket("yyyy-88888", "test-bucket-2", "B2", Map.empty[String, String]))

  def mockFiles(): List[File] = List(singleMockFile, File("test-file-2", "456def", FileTypeEnumeration.VIDEO))

  def singleMockFile(): File = File("test-file-1", "123abc", FileTypeEnumeration.VIDEO)

}