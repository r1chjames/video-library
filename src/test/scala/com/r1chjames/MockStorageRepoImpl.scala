package com.r1chjames

import java.io.InputStream
import java.time.LocalDateTime

import com.r1chjames.models.{Bucket, File}
import com.r1chjames.repos.StorageRepo

class MockStorageRepoImpl extends StorageRepo {

  def withKeys(accountId: String, applicationKey: String): this.type = this

  def listFilesForDate(bucketId: String, date: LocalDateTime): List[File] = null

  def downloadFile(fileId: String): Unit = {}

  def listFilesInBucket(bucket: String): List[File] = null

  def listBuckets: List[Bucket] = null

  override def streamFile(fileId: String): (InputStream, File) = null
}


