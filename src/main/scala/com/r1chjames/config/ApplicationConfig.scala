package com.r1chjames.config

import java.io.{File, FileInputStream}

import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.Constructor

object ApplicationConfig {

  def apply(): Configuration = loadConfig()

  def loadConfig(): Configuration = {
    val input = new FileInputStream(new File("src/main/resources/local_config.yml"))
    val yaml = new Yaml(new Constructor(classOf[Configuration]))
    yaml.load(input).asInstanceOf[Configuration]
  }

}
