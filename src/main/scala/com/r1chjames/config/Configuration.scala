package com.r1chjames.config

import scala.beans.BeanProperty

class Configuration {

  @BeanProperty var accountId = ""
  @BeanProperty var applicationKey = ""
  @BeanProperty var endpoint = ""
  @BeanProperty var userAgent = ""
  @BeanProperty var downloadLocation = ""
  @BeanProperty var apiPrefix = "/api/v1"

}
