package com.r1chjames.components

import com.r1chjames.services.StorageServ

trait StorageServComponent {
  val storageServ: StorageServ
}
