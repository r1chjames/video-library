package com.r1chjames.components

import com.r1chjames.repos.{B2StorageRepoImpl, StorageRepo}

trait MasterComponentImpl extends MasterComponent {
  val storageRepo: StorageRepo = new B2StorageRepoImpl()
}