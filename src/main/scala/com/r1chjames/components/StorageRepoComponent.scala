package com.r1chjames.components

import com.r1chjames.repos.StorageRepo

trait StorageRepoComponent {
  val storageRepo: StorageRepo
}
