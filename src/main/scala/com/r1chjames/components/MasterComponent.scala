package com.r1chjames.components

import com.r1chjames.repos.StorageRepo

trait MasterComponent {
  val storageRepo: StorageRepo
}