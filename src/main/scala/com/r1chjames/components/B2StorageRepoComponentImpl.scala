package com.r1chjames.components
import com.r1chjames.repos.{B2StorageRepoImpl, StorageRepo}

trait B2StorageRepoComponentImpl extends StorageRepoComponent {
  val storageRepo: StorageRepo = new B2StorageRepoImpl

}
