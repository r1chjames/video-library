package com.r1chjames.components
import com.r1chjames.services.{StorageServImpl, StorageServ}

trait B2StorageServComponentImpl extends StorageServComponent {
  self: StorageRepoComponent =>
  val storageServ: StorageServ = new StorageServImpl(storageRepo)
}
