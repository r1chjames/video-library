package com.r1chjames.logging

import org.slf4j.LoggerFactory

object LogMessages {

  val log = LoggerFactory.getLogger(getClass)

  def logTimeTaken(timeTaken: Long): Unit = {
    log.info(s"Took $timeTaken milliseconds")
  }

}
