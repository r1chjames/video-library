package com.r1chjames.services

import java.io.{InputStream, OutputStream}
import java.time.LocalDateTime

import com.r1chjames.models.{Bucket, File}
import com.r1chjames.repos.StorageRepo

class StorageServImpl(storageRepo: StorageRepo) extends StorageServ {

  def listFilesForDate(bucketId: String, date: LocalDateTime): List[File] = storageRepo.listFilesForDate(bucketId, date)

  def downloadFile(fileId: String): Unit = storageRepo.downloadFile(fileId)

  def streamFile(fileId: String): (InputStream, File) = storageRepo.streamFile(fileId)

  def listFilesInBucket(bucket: String): List[File] = storageRepo.listFilesInBucket(bucket)

  def listBuckets(): List[Bucket] = storageRepo.listBuckets

}
