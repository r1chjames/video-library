package com.r1chjames.services

import java.io.{InputStream, OutputStream}
import java.time.LocalDateTime

import com.r1chjames.models.{Bucket, File}

trait StorageServ {

  def listFilesForDate(bucketId: String, date: LocalDateTime): List[File]

  def downloadFile(fileId: String): Unit

  def streamFile(fileId: String): (InputStream, File)

  def listFilesInBucket(bucket: String): List[File]

  def listBuckets: List[Bucket]

}
