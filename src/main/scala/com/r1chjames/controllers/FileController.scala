package com.r1chjames.controllers

import java.time.LocalDateTime

import akka.stream.scaladsl.{Source, StreamConverters}
import akka.util.ByteString
import com.r1chjames.components.MasterComponentImpl
import com.r1chjames.models.{Bucket, File, FileMetaData}
import com.r1chjames.utils.DateTimeFormaters
import controllers.AssetsFinder
import javax.inject._
import play.api.http.HttpEntity
import play.api.libs.json.Json
import play.api.libs.json.Json.toJson
import play.api.mvc._

@Singleton
class FileController @Inject()(cc: ControllerComponents)(implicit assetsFinder: AssetsFinder)
  extends AbstractController(cc) with MasterComponentImpl {

  implicit val bucketWriter = Json.writes[Bucket]
  implicit val fileMetaDataWriter = Json.writes[FileMetaData]
  implicit val fileWriter = Json.writes[File]

  def listBuckets() = Action { implicit request =>
    val (accountId, applicationKey) = parseHeaders(request)

    Result(
      header = ResponseHeader(OK, Map.empty),
      body = HttpEntity.Strict(
        ByteString(
          Json.prettyPrint(
            toJson(storageRepo
              .withKeys(accountId, applicationKey)
              .listBuckets)))
        , Some("application/json"))
    ).enableCors
  }

  def listFilesInBucket(bucketId: String) = Action { implicit request =>
    val (accountId, applicationKey) = parseHeaders(request)

    Result(
      header = ResponseHeader(OK, Map.empty),
      body = HttpEntity.Strict(
        ByteString(
          Json.prettyPrint(
            toJson(storageRepo
              .withKeys(accountId, applicationKey)
            .listFilesInBucket(bucketId))))
        , Some("application/json"))
    ).enableCors
  }

  def listFilesForDate(bucketId: String, date: String) = Action { implicit request =>
    val (accountId, applicationKey) = parseHeaders(request)
    Result(
      header = ResponseHeader(OK, Map.empty),
      body = HttpEntity.Strict(
        ByteString(
          Json.prettyPrint(
            toJson(storageRepo.withKeys(accountId, applicationKey)
              .listFilesForDate(bucketId, LocalDateTime.parse(date, DateTimeFormaters.apiDateTimeFormat)))))
        , Some("application/json"))
    ).enableCors
  }

  def streamFile(bucketId: String, fileId: String) = Action { implicit request =>

    val (accountId, applicationKey) = parseHeaders(request)
    val (fileStream, metaData) = storageRepo
      .withKeys(accountId, applicationKey)
      .streamFile(fileId)

    val source: Source[ByteString, _] = StreamConverters.fromInputStream(() => fileStream)

    Result(
      header = ResponseHeader(OK, Map(
        CONTENT_RANGE -> s"bytes */${metaData.metaData.contentLength}",
        ACCEPT_RANGES -> "bytes",
        PRAGMA -> "public",
        CONTENT_TRANSFER_ENCODING -> "binary",
        CONTENT_DISPOSITION -> "attachment"
      )),
    body = HttpEntity.Streamed(source, Some(metaData.metaData.contentLength), Some("video/mp4"))
    ).enableCors
  }

  def downloadFile(bucketId: String, fileId: String) = Action { implicit request =>
    val (accountId, applicationKey) = parseHeaders(request)

    storageRepo.withKeys(accountId, applicationKey).downloadFile(fileId)
    Result(
      header = ResponseHeader(ACCEPTED, Map.empty),
      body = HttpEntity.NoEntity
    ).enableCors
  }

  def parseHeaders(request: Request[AnyContent]): (String, String) = {
    (request.headers.get("accountId").getOrElse(throw new RuntimeException),
    request.headers.get("applicationKey").getOrElse(throw new RuntimeException))
  }

  implicit class RichResult (result: Result) {
    def enableCors =  result.withHeaders(
      "Access-Control-Allow-Origin" -> "*"
      , "Access-Control-Allow-Methods" -> "OPTIONS, GET, POST, PUT, DELETE, HEAD"   // OPTIONS for pre-flight
      , "Access-Control-Allow-Headers" -> "Accept, Content-Type, Origin, X-Json, X-Prototype-Version, X-Requested-With" //, "X-My-NonStd-Option"
      , "Access-Control-Allow-Credentials" -> "true"
    )
  }

}
