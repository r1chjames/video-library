package com.r1chjames.controllers

import com.r1chjames.config.Configuration
import controllers.Assets
import javax.inject.Inject
import play.api.http.HttpErrorHandler
import play.api.mvc._

class HomeController @Inject()(assets: Assets, cc: ControllerComponents, config: Configuration, errorHandler: HttpErrorHandler) extends AbstractController(cc) {

  def index: Action[AnyContent] = assets.at("index.html")

  def assetOrDefault(resource: String): Action[AnyContent] = if (resource.startsWith(config.apiPrefix)){
    Action.async(r => errorHandler.onClientError(r, NOT_FOUND, "Not found"))
  } else {
    if (resource.contains(".")) assets.at(resource) else index
  }
}
