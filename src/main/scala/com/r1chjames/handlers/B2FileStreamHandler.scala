package com.r1chjames.handlers

import java.io.InputStream

import com.backblaze.b2.client.contentSources.B2Headers
import org.apache.commons.io.IOUtils

class B2FileStreamHandler extends B2FileHandler {

  var fileStream:InputStream = _

  override def readContent(responseHeaders: B2Headers, in: InputStream): Unit = {
    log.info(s"Streaming file: ${responseHeaders.getFileNameOrNull}")

    fileStream = IOUtils.toBufferedInputStream(in)
  }

}
