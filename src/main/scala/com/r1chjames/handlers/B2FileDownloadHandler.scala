package com.r1chjames.handlers

import java.io.{FileOutputStream, InputStream}

import com.backblaze.b2.client.contentSources.B2Headers
import com.r1chjames.config.ApplicationConfig

class B2FileDownloadHandler extends B2FileHandler {

  override def readContent(responseHeaders: B2Headers, in: InputStream): Unit = {
    var out = None: Option[FileOutputStream]
    var downloadLocation = ApplicationConfig().getDownloadLocation
    val fileName = responseHeaders.getFileNameOrNull.replace("/","")

    if (!downloadLocation.charAt(downloadLocation.length-1).equals('/')) {
      downloadLocation += '/'
    }
    downloadLocation += fileName

    log.info(s"Downloading file: $fileName to location: $downloadLocation")

    out = Some(new FileOutputStream(downloadLocation))
    var c = 0
    while ({c = in.read; c != -1}) {
      out.get.write(c)
    }
  }
}
