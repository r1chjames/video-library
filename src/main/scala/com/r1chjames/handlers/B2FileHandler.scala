package com.r1chjames.handlers

import java.io.{FileOutputStream, InputStream}

import com.backblaze.b2.client.contentHandlers.B2ContentSink
import com.backblaze.b2.client.contentSources.B2Headers
import org.slf4j.LoggerFactory

trait B2FileHandler extends B2ContentSink {
  val log = LoggerFactory.getLogger(getClass)

  override def readContent(responseHeaders: B2Headers, in: InputStream): Unit = {

  }
}
