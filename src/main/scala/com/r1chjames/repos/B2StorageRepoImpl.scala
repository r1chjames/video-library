package com.r1chjames.repos

import java.io.{InputStream, OutputStream}
import java.time.LocalDateTime

import com.backblaze.b2.client.B2StorageClient
import com.backblaze.b2.client.structures.{B2FileVersion, B2ListFileNamesRequest}
import com.backblaze.b2.client.webApiHttpClient.B2StorageHttpClientBuilder
import com.r1chjames.config.ApplicationConfig
import com.r1chjames.handlers.{B2FileDownloadHandler, B2FileStreamHandler}
import com.r1chjames.models.{Bucket, File, FileMetaData, FileTypeEnumeration}
import com.r1chjames.utils.{DateTimeFormaters, Stopwatch}

import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer

class B2StorageRepoImpl extends StorageRepo {

  var accId: String = _
  var appKey: String = _

  def withKeys(accountId: String, applicationKey: String): this.type = {
    accId = accountId
    appKey = applicationKey
    this
  }

  def listFilesForDate(bucketId: String, date: LocalDateTime): List[File] = {
    val stopwatch = new Stopwatch
    val fileNameRequest = B2ListFileNamesRequest.builder(bucketId).setPrefix(date.format(DateTimeFormaters.b2DateTimeFormat)).build()

    var files = ListBuffer[File]()
    var b2Client = getClient()
    b2Client.fileNames(fileNameRequest)
      .forEach(f => {
        val fileUrl: String = b2Client.getDownloadByIdUrl(f.getFileId)
        files += File(f.getFileName, f.getFileId, FileTypeEnumeration.VIDEO, processMetaData(f), fileUrl)
      })
    stopwatch.logTimeTaken()
    files.toList
  }

  def downloadFile(fileId: String): Unit = {
    val stopwatch = new Stopwatch
    val handler = new B2FileDownloadHandler
    getClient().downloadById(fileId, handler)
    stopwatch.logTimeTaken()
  }

  def streamFile(fileId: String):  (InputStream, File) = {
    val stopwatch = new Stopwatch
    val handler = new B2FileStreamHandler
    val client = getClient()
    val file = client.getFileInfo(fileId)
    client.downloadById(fileId, handler)
    stopwatch.logTimeTaken()
    handler.fileStream -> File(file.getFileName, file.getFileId, FileTypeEnumeration.VIDEO, processMetaData(file), null)
  }

  def listFilesInBucket(bucket: String): List[File] = {
    val stopwatch = new Stopwatch
    var files = ListBuffer[File]()
    var b2Client = getClient()
    b2Client.fileNames(bucket)
      .forEach(f => {
        val fileUrl: String = b2Client.getDownloadByIdUrl(f.getFileId)
        files += File(f.getFileName, f.getFileId, FileTypeEnumeration.VIDEO, processMetaData(f), fileUrl)
      })
    stopwatch.logTimeTaken()
    files.toList
  }

  def listBuckets: List[Bucket] = {
    val stopwatch = new Stopwatch
    val buckets = ListBuffer[Bucket]()
    getClient().listBuckets().getBuckets
        .forEach(b => buckets += Bucket(b.getBucketId, b.getBucketName, b.getBucketType, b.getBucketInfo.asScala.toMap))
    stopwatch.logTimeTaken()
    buckets.toList
  }

  def getClient(): B2StorageClient = {
    val config = ApplicationConfig()
    B2StorageHttpClientBuilder.builder(accId,
      appKey,
      config.userAgent).build()
  }

  def processMetaData(file: B2FileVersion): FileMetaData = {
    FileMetaData(file.getContentLength, file.getContentType, file.getContentSha1, file.getFileInfo.asScala, file.getUploadTimestamp)
  }
}
