package com.r1chjames.utils

import java.time.format.DateTimeFormatter

object DateTimeFormaters {

  val b2DateTimeFormat = DateTimeFormatter.ofPattern("yyyy'Y'MM'M'dd'D'HH'H'")
  val apiDateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH")

}
