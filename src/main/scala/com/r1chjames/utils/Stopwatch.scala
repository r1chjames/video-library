package com.r1chjames.utils

import org.slf4j.LoggerFactory

class Stopwatch {

  val log = LoggerFactory.getLogger(getClass)
  val startTime = System.currentTimeMillis()

  def getElapsedTime(): Long = {
    System.currentTimeMillis() - startTime
  }

  def logTimeTaken(operation: String): Unit = {
    log.info(s"$operation took $getElapsedTime ms")
  }

  def logTimeTaken(): Unit = {
    val callingClass = Thread.currentThread.getStackTrace()(2).getClassName
    val callingFunction = Thread.currentThread.getStackTrace()(2).getMethodName
    log.info(s"$callingClass.$callingFunction took $getElapsedTime ms")
  }

}
