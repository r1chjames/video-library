package com.r1chjames.models

case class Bucket(bucketId: String, bucketName: String, bucketType: String, bucketInfo: Map[String, String]) {

}