package com.r1chjames.models

import scala.collection.mutable

case class FileMetaData(contentLength: Long, contentType: String, contentSha1: String, fileInfo: mutable.Map[String, String], uploadTimestamp: Long) {

}
