package com.r1chjames.models

case class File(filename: String, fileId: String, fileType: FileTypeEnumeration.Value, metaData: FileMetaData, url: String) {

}
