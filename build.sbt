name := "video-library"

version := "0.1"

scalaVersion := "2.12.6"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

disablePlugins(PlayLayoutPlugin)
PlayKeys.playMonitoredFiles ++= (sourceDirectories in (Compile, TwirlKeys.compileTemplates)).value
scriptClasspath += "/Users/rja01/IdeaProjects/video-library/src/main/resources"

libraryDependencies ++= Seq(
  "org.yaml" % "snakeyaml" % "1.21",
  "org.slf4j" % "slf4j-api" % "1.7.5",
  "ch.qos.logback" % "logback-classic" % "1.2.3",

  "com.typesafe.play" %% "play" % "2.6.14",
  "com.typesafe.play" %% "play-iteratees" % "2.6.1",
  guice,
  "commons-io" % "commons-io" % "2.6",

  "com.backblaze.b2" % "b2-sdk-core" % "1.4.0",
  "com.backblaze.b2" % "b2-sdk-httpclient" % "1.4.0",

  "org.scalactic" %% "scalactic" % "3.0.5",
  "org.scalatest" %% "scalatest" % "3.0.5" % Test,
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.1" % Test,
  "org.mockito" % "mockito-all" % "2.0.2-beta" % Test
)